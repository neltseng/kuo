<?php

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

// Remove the WooThemes Helper notice from the admin
add_action( 'init', 'cw_remove_function' );
function cw_remove_function() {

    // REMOVE WP EMOJI
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');
    
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');

    remove_action( 'admin_notices', 'woothemes_updater_notice' );

}

// Remove WP trash 
remove_action( 'wp_head', 'wp_generator' ) ; 
remove_action( 'wp_head', 'wlwmanifest_link' ) ; 
remove_action( 'wp_head', 'rsd_link' ) ;
remove_action( 'wp_head', 'feed_links', 2 ); 
remove_action( 'wp_head', 'feed_links_extra', 3 );
//add_filter('show_admin_bar', '__return_false');

// Disable use XML-RPC
add_filter( 'xmlrpc_enabled', '__return_false' );

// Disable X-Pingback to header
add_filter( 'wp_headers', 'disable_x_pingback' );
function disable_x_pingback( $headers ) {
    unset( $headers['X-Pingback'] );

return $headers;
}


//Remove query strings
add_filter( 'script_loader_src', 'cw_remove_script_version', 15, 1 ); 
add_filter( 'style_loader_src', 'cw_remove_script_version', 15, 1 );
function cw_remove_script_version( $src ){
	return remove_query_arg( 'ver', $src );
} 



// Optimize WooCommerce Scripts
add_action('wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99);
function child_manage_woocommerce_styles() {
    //remove generator meta tag
    remove_action('wp_head', array($GLOBALS['woocommerce'], 'generator'));

    //first check that woo exists to prevent fatal errors
    if (function_exists('is_woocommerce')) {
        //dequeue scripts and styles
        if (!is_woocommerce() && !is_cart() && !is_checkout()) {
            wp_dequeue_style('woocommerce_frontend_styles');
            wp_dequeue_style('woocommerce_fancybox_styles');
            wp_dequeue_style('woocommerce_chosen_styles');
            wp_dequeue_style('woocommerce_prettyPhoto_css');
            wp_dequeue_script('wc_price_slider');
            wp_dequeue_script('wc-single-product');
            wp_dequeue_script('wc-add-to-cart');
            wp_dequeue_script('wc-checkout');
            wp_dequeue_script('wc-add-to-cart-variation');
            wp_dequeue_script('wc-single-product');
            wp_dequeue_script('wc-cart');
            wp_dequeue_script('wc-chosen');
            wp_dequeue_script('woocommerce');
            wp_dequeue_script('prettyPhoto');
            wp_dequeue_script('prettyPhoto-init');
            wp_dequeue_script('jquery-blockui');
            wp_dequeue_script('jquery-placeholder');
            wp_dequeue_script('fancybox');
            wp_dequeue_script('jqueryui');
        }
    }

}

// Theme Settings
remove_action( 'avia_add_to_cart', 'woocommerce_template_single_add_to_cart', 30, 2 );
remove_action('woocommerce_pagination', 'woocommerce_catalog_ordering', 20 );


// Auto category thumbnail
/*
class SB_WC_Auto_Category_Thumbnails {

    public function __construct() {
        add_action( 'plugins_loaded', array( $this, 'remove_default' ) );
        add_action( 'woocommerce_before_subcategory_title', array( $this, 'auto_category_thumbnail' ) );
        add_action( 'woocommerce_settings_tabs_sbo_wcact', array( $this, 'settings_tab' ) );
        add_action( 'woocommerce_update_options_sbo_wcact', array( $this, 'update_settings' ) );

        add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_tab' ), 50 );
    }

    public function remove_default() {
        remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail' );
    }

    public function auto_category_thumbnail( $cat ) {

        //If a thumbnail is explicitly set for this category, we don't need to do anything else
        if ( get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ) ) {
                woocommerce_subcategory_thumbnail( $cat );
                return;
        }

        //Otherwise build our query
        $query_args = array(
            'meta_query' => array(
                array(
                    'key' => '_visibility',
                    'value' => array( 'catalog', 'visible' ),
                    'compare' => 'IN'
                )
            ),
            'post_status' => 'publish',
            'post_type' => 'product',
            'posts_per_page' => 1,
            'tax_query' => array(
                array(
                    'field' => 'id',
                    'taxonomy' => 'product_cat',
                    'terms' => $cat->term_id
                )
            )
        );

        $wcact_settings = get_option( 'wcact_settings' );

        //Random or latest image?
        $query_args['orderby'] = $wcact_settings['orderby'];

        //Query DB
        $product = get_posts( $query_args );

        //If matching product found, check for a thumbnail, otherwise fall back
        if( $product && has_post_thumbnail( $product[0]->ID ) ) {
            echo get_the_post_thumbnail( $product[0]->ID, 'shop_catalog' );
        } else {
            woocommerce_subcategory_thumbnail( $cat );
            return;
        }
    }

    function add_settings_tab( $settings_tabs ) {
        $settings_tabs['sbo_wcact'] = __( 'Auto Category Thumbnails', 'wc-auto-category-thumbnails' );
        return $settings_tabs;
    }

    function get_settings() {

        $settings = array(
            'section_title' => array(
                'name'     => __( 'WC Auto Category Thumbnails', 'woocommerce-settings-tab-demo' ),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wcact_settings[title]',
            ),
            'orderby' => array(
                'name' => __( 'Image to use', 'woocommerce-settings-tab-demo' ),
                'type' => 'radio',
                'desc' => __( 'Which product image should be displayed for each category?', 'woocommerce-settings-tab-demo' ),
                'std' => 'rand',
                'id'   => 'wcact_settings[orderby]',
                'options' => array(
                    'rand' => 'Random',
                    'date' => 'Latest',
                ),
            ),
            'section_end' => array(
                 'type' => 'sectionend',
                 'id' => 'wcact_settings[sectionend]',
            )
        );
        return apply_filters( 'wcact_settings', $settings );
    }

    function settings_tab() {
        woocommerce_admin_fields( $this->get_settings() );
    }

    function update_settings() {
        woocommerce_update_options( $this->get_settings() );
    }

}

new SB_WC_Auto_Category_Thumbnails();
*/
