<?php
/**
 *  FB Pixel Tracking Code
 * 
 */

if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

// Full Site
add_action( 'wp_footer', 'full_fb_pixeltracking' );

function full_fb_pixeltracking() {
      global $post,$woocommerce;
      $product = get_product(get_the_ID());
      $product_id = $post->ID;
      $search_query = get_search_query();
      /**
       *
       * value: '<?php echo $product->get_price() ?>' , currency: 'TWD' 
       * fbq('track', 'AddPaymentInfo', {value: '<?php echo WC()->cart->total ?>', currency: 'TWD'}); 
       * 
       */

      
  if ( wp_script_is( 'jquery', 'done' ) && !is_product() && !is_search() && !is_cart() && !is_checkout() && !is_wc_endpoint_url( 'order-received' ) ) {
    ?><!-- Facebook Conversion Code for Flightine -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1091682747627692');
      fbq('track', "PageView");
      </script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1091682747627692&ev=PageView&noscript=1"
      /></noscript>
      <!-- END FB Tracking -->
  <?php
  }
 
      elseif ( is_product() ) {

        ?><!-- Facebook Conversion Code for Product -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1091682747627692');
      fbq('track', 'PageView');
      fbq('track', 'ViewContent', {
        content_name: '<?php echo the_title() ?>',
        content_ids: ['<?php echo $product_id ?>'],
        content_type: 'product',
       });
      </script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1091682747627692&ev=PageView&noscript=1"
      /></noscript>
      <!-- END FB Tracking -->
      <?php
      } 

  elseif ( is_search() ) {

    ?><!-- Facebook Conversion Code for Search -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1091682747627692');
      fbq('track', "PageView");
      fbq('track', 'Search', {search_string: '<?php echo $search_query ?>'});</script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1091682747627692&ev=PageView&noscript=1"
      /></noscript>
      <!-- END FB Tracking -->
  <?php
  } 

  elseif ( is_cart() ) {

    ?><!-- Facebook Conversion Code for Cart -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1091682747627692');
      fbq('track', "PageView");
      fbq('track', 'InitiateCheckout');</script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1091682747627692&ev=PageView&noscript=1"
      /></noscript>
      <!-- END FB Tracking -->
  <?php
  }

  elseif ( is_checkout() ) {

    ?><!-- Facebook Conversion Code for Checkout -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1091682747627692');
      fbq('track', "PageView");
      fbq('track', 'AddPaymentInfo');</script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1091682747627692&ev=PageView&noscript=1"
      /></noscript>
      <!-- END FB Tracking -->
  <?php
  }
}


// Thnak You Page
add_action( 'woocommerce_thankyou', 'fb_pixeltracking' );

function fb_pixeltracking( $order_id ) {
   $order = new WC_Order( $order_id );
   $order_total = $order->get_total();
 ?>
  <!-- Facebook Conversion Code for Purchase -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '1091682747627692');
      fbq('track', "PageView");
      fbq('track', 'Purchase', {content_type: 'product',value: '<?php echo $order_total ?>', currency: 'TWD'});</script>
      <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1091682747627692&ev=PageView&noscript=1"/></noscript>
  <!-- END FB Tracking -->
<?php
 }

 // Facebook Tracking Pixel End //