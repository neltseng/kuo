<?php

/*
Plugin Name: Cloudwp Admin Theme
Plugin URI: https://cloudwp.pro/
Description: For WooCommerce 商店後台管理主題
Author: Nel Tseng
Version: 0.1
Author URI: https://cloudwp.pro/
*/

if ( ! defined( 'ABSPATH' ) ) { 
    exit;
}

include_once ('inc/class-optimize.php');
include_once ('inc/class-fb-pixel.php');

add_action('admin_enqueue_scripts', 'cwp_wooatheme_style');
add_action('login_enqueue_scripts', 'cwp_wooatheme_style');
add_action('login_head', 'cwp_wooatheme_login');
add_filter('post_date_column_time','cwp_wooatheme_order_time_style',11,2);
add_action('admin_bar_menu', 'cwp_wooatheme_remove_wp_logo', 999 );
add_action('admin_init', 'remove_dashboard_meta');
add_filter('admin_footer_text', 'left_cwp_wooatheme_footer'); 


function cwp_wooatheme_style() {
    wp_enqueue_style('cwp_wooatheme_style', '//statics.woocloud.io/assets/woo-adm/css/admin.css');
}

function cwp_wooatheme_login() {
  echo '<link rel="stylesheet" type="text/css" href="//statics.woocloud.io/assets/woo-adm/css/login.css">';
}


// change default time formart display on the order list page
function cwp_wooatheme_order_time_style($h_time, $post){

    global $post;
    $type=get_post_type($post);
    if('shop_order'==$type)
        $h_time   = get_the_time( 'Y-m-d H:i:s', $post );
    return $h_time;
}

function cwp_wooatheme_remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}

// remove dashboard widgets
function remove_dashboard_meta() {
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
	remove_meta_box('dashboard_primary', 'dashboard', 'side');
	remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
}

// left side footer text
function left_cwp_wooatheme_footer($text) {
    $text = '<cwp-footer>This theme was made by <a href="https://cloudwp.pro/">cloudwp</a>.</cwp-footer>';
    return $text;
}

