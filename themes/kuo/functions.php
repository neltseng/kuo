<?php
	
	/* Styles
	=============================================================== */
	
	function nm_child_theme_styles() {
		 // Enqueue child theme styles
		 wp_enqueue_style( 'nm-child-theme', get_stylesheet_directory_uri() . '/style.css' );
	}
	add_action( 'wp_enqueue_scripts', 'nm_child_theme_styles', 1000 ); // Note: Use priority "1000" to include the stylesheet after the parent theme stylesheets


	add_action('wp_head', 'homepage_assets', 5);
	function homepage_assets() {
	    if (is_front_page()) {
?>
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" />
<meta property="og:title" content="果真新鮮 – 健康生活每一天" />
<meta property="og:description" content="" />
<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/images/og-kuofresh.jpg" />

<?php
	        wp_deregister_style('nm-nm_styles');
			wp_deregister_style('normalize');
			wp_deregister_style('slick-slider');
			wp_deregister_style('magnific-popup');    
			//wp_deregister_style('nm-grid');
			//wp_deregister_style('nm-core');
			wp_deregister_style('selectod');
			//wp_deregister_style('nm-shop');
			wp_deregister_style('nm_webfonts');
			wp_deregister_style('nm-elements');
			wp_dequeue_script('modernizr');
			wp_dequeue_script('unveil');
			wp_dequeue_script('slick-slider');
			wp_dequeue_script('magnific-popup');
			wp_dequeue_script('packery');

	    }
	}



// Change Empty Cart Button url
add_filter('woocommerce_return_to_shop_redirect', 'cw_change_empty_cart_button_url');
function cw_change_empty_cart_button_url() {
    return get_site_url();
    
}
// Rename Billing City
add_filter('woocommerce_billing_fields', 'cw_custom_billing_fields');
function cw_custom_billing_fields($fields) {
    $fields['billing_city']['label'] = '鄉 / 鎮 / 市 / 區';
    return $fields;
}

