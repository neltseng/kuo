<?php
/**
 * Home Page
 *
 * Template Name: Home Page
 *
 * @package cloudwp
 */

get_header(); ?>

	
	<meta name="google-site-verification" content="-MLXb98KIXlOE_dmJ18CK9Ynil6NnPMRct9vU1ZYDpg" />
	<!--css-->
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/global.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/athlete.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora|Roboto:700,500" rel="stylesheet">
	<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/html5shiv.min.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/respond.min.js"></script>
	<![endif]-->
	<!--GA-->
	<script src="https://use.typekit.net/oem8boq.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>


	
	<div id="section-1" class="">
		<div class="container">
			<h2 class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/KUO_logo.png" alt=""></h2>
			<div class="row">
				<div class="col-sm-6">
					<div class="title-group">
						<h2>感恩生命中的每次相遇</h2>
						<h3>單筆消費滿<span>3000</span>，即享<span>免運費</span></h3>
						<h3>單筆消費滿<span>5000</span>，即享<span>95折優惠</span></h3>
					</div>
				</div>
				<div class="col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pear.png" alt="果真新鮮 新春好禮" title="果真新鮮 新春好禮" class="pear img-responsive">
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="section-2" class="">
		<div class="container">
			<div class="col-lg-4 col-sm-6">
				<div class="title-group">
					<h2><span>果真新鮮</span><span>健康生活每一天</span></h2>
					<h3>等到霧霾來了，才發現世道變了<br>
						原以為幸福是有錢有名有地位<br>
						現在只希望有健康</h3>
				</div>
			</div>
		</div>
	</div>
	<div id="section-3" class="">
		<div class="container">
			<div class="col-md-4 col-sm-6">
				<div class="card">
					<a href="/shop/product1/">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Product1.jpg" alt="韓國富士蘋果" title="韓國富士蘋果" class="">
					</a>
					<div class="pro-info">
						<h4>韓國富士蘋果<span>（6入）</span></h4>
						<span class="price"><span>$</span>670</span>
						<h5>韓國直送：完整保存新鮮品質，彷彿剛摘下般新鮮 <br>
							驚艷口感：絕佳酸甜滋味，老少咸宜<br>
							精選外觀：粉嫩外觀，年節送禮最佳選擇</h5>
					</div>
					<div class="btn-lg add-to-cart">
						<a href="<?php echo get_site_url(); ?>/?add-to-cart=11"><i class="fa fa-cart-plus" aria-hidden="true"></i>加入購物車</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="card">
					<a href="/shop/product2/">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Product2.jpg" alt="韓國直送高山梨" title="韓國直送高山梨" class="">
					</a>
					<div class="pro-info">
						<h4>韓國直送高山梨<span>（5入）</span></h4>
						<span class="price"><span>$</span>670</span>
						<h5>韓國直送：完整保存新鮮品質，彷彿剛摘下般新鮮<br>
							驚艷口感：口感鮮甜、Juicy 多汁<br>
							精選外觀：年節送禮最佳選擇</h5>
					</div>
					<div class="btn-lg add-to-cart">
						<a href="<?php echo get_site_url(); ?>/?add-to-cart=13"><i class="fa fa-cart-plus" aria-hidden="true"></i>加入購物車</a>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="card">
					<a href="/shop/product3/">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Product3.jpg" alt="韓國直送高山梨" title="韓國直送高山梨" class="">
					</a>
					<div class="pro-info">
						<h4>韓國富士蘋果<span>（8入）</span></h4>
						<span class="price"><span>$</span>1350</span>
						<h5>韓國直送：完整保存新鮮品質，彷彿剛摘下般新鮮<br>
							驚艷口感：口感鮮甜、Juicy 多汁<br>
							精選外觀：年節送禮最佳選擇</h5>
					</div>
					<div class="btn-lg add-to-cart">
						<a href="<?php echo get_site_url(); ?>/?add-to-cart=17"><i class="fa fa-cart-plus" aria-hidden="true"></i>加入購物車</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="in-cart">
		<span>2</span>
		<a href="#"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>
	</div>
	<footer>
		<nav>
			<ul>
				<li><a href="#">網站使用條款</a></li>
				<li><a href="#">隱私權條款</a></li>
			</ul>
		</nav>
	</footer>
	
	<!--jQuery--> 
	<script type="text/javascript" src="js/jquery-2.2.4.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="js/script.js" ></script>

<?php get_footer() ?>
