<?php

/**
 * Email Header
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates/Emails
 * @version     2.4.0
 */

if (!defined('ABSPATH')) {
    exit;
     // Exit if accessed directly
    
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo get_bloginfo('name'); ?></title>
    </head>
    <body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <div id="wrapper">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <a href="#">
                        <div id="template_header_image" style="margin-top: -45px ! important; padding-top: 7px! important; display: block;">
                            <?php
                                if ($img = get_option('woocommerce_email_header_image')) {
                                    echo '<p style="padding-top: 7px ! important; margin-top: 0px;text-align: left;}"><img src="' . esc_url($img) . '" alt="' . get_bloginfo('name') . '" /></p>';
                                }
                            ?>
                        </a>
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" width="680" id="template_container">
                            <tr>
                                <td align="center" valign="top">
                                    <!-- Header -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="680" id="template_header">
                                        <tr>
                                            <td>
                                                <h1><?php
echo $email_heading; ?></h1>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- End Header -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <!-- Body -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="680" id="template_body">
                                        <tr>
                                            <td valign="top" id="body_content">
                                                <!-- Content -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <div id="body_content_inner">
